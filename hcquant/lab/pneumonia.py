from pyecharts.charts import Map
from pyecharts import options as opts
import requests
import json
import re

def pneumonia():
    # 爬虫读取新浪
    result = requests.get('https://interface.sina.cn/news/wap/fymap2020_data.d.json?1580097300739&&callback=sinajp_1580097300873005379567841634181')
    jsonstr = re.search("\(+([^)]*)\)+", result.text).group(1)
    html = f"{jsonstr}"
    table = json.loads(f"{html}")
    # 获取省份与病例信息
    data=[]
    for province in table['data']['list']:
        data.append((province['name'], province['value']))
    # 作图
    map_p = Map() # 初始化map
    map_p.set_global_opts(title_opts=opts.TitleOpts(title="实时疫情图"), visualmap_opts=opts.VisualMapOpts(max_=200))
    map_p.add("确诊病例", data, maptype="china")
    map_p.render("ncov.html")  # 生成html文件


    # 调用百度接口
    name = []
    value = []
    for province in table['data']['list']:
        data.append((province['name'], province['value']))
        name.append(province['name'])
        value.append(province['value'])

    from pyecharts.charts import BMap

    c = (
            BMap()
            .add_schema(
                baidu_ak="mcH6sBNaAfsbkSndFI5zO90j9wUpRMFy",
                center=[120.13066322374, 30.240018034923],
             )
            .add(
                "bmap",
                [list(z) for z in zip(name, value)],
                label_opts=opts.LabelOpts(formatter="{b}"),
            )
            .set_global_opts(title_opts=opts.TitleOpts(title="BMap-基本示例"))
        )

    c.render('ncov_bd.html')
    return data