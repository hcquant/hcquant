import pandas as pd
import requests
from bs4 import BeautifulSoup

# id='230407198503150123'
def getidinfo(id):
    url ='http://qq.ip138.com/idsearch/index.asp?userid={0}&action=idcard'.format(id)
    web = requests.get(url)
    web.encoding='UTF8'
    soup=BeautifulSoup(web.text)
    res='';
    for k in soup.select('td'):
        res = res+k.text+','

    return res
