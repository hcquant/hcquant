import json
import pandas as pd
import requests
from ..utils import *
import datetime as dt
today = dt.datetime.now().strftime('%Y%m%d')

def get_vix(code='000016'):
    web=requests.get('http://service.hcquant.com/production/api/vix.php?code={0}'.format(code))
    web.encoding='UTF-8-sig'
    content=web.text
    content=content.replace('\n','')
    content=content.replace(' ','')
    a=eval(content)
    print('Already Download VIX into {0}'.format(HOME_PATH+r'\vix'))
    r=pd.DataFrame(a)
    r.to_csv(HOME_PATH+r'\vix\hcvix_{0}_{1}.csv'.format(code,today))
    return


