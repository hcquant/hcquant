import json
import pandas as pd
import requests
from ..utils import *


def get_master_performace():
    web=requests.get('http://service.hcquant.com/production/api/master.php')
    web.encoding='UTF-8-sig'
    content=web.text
    content=content.replace('\n','')
    content=content.replace(' ','')
    a=eval(content)
    print('Already Download MasterData into {0}'.format(HOME_PATH+r'\master'))
    r=pd.DataFrame(a)
    r.to_csv(HOME_PATH+r'\master\masterperformance.csv')
    return r


