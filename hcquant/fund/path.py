# hcquant
# 指定工作路径

import os

# 打包时候写死
PYTHON_ROOT = "C:/ProgramData/Anaconda3/python.exe"

# 工作路径
HOME_PATH = os.path.expanduser(f'~{os.sep}hcquant')

FUND_PATH = os.path.join(HOME_PATH, 'fund')
if not os.path.exists(FUND_PATH):
    os.makedirs(FUND_PATH)

LOG_PATH = os.path.join(HOME_PATH, 'syslog')
if not os.path.exists(LOG_PATH):
    os.makedirs(LOG_PATH)

NEWS_PATH = os.path.join(HOME_PATH, 'news')
if not os.path.exists(NEWS_PATH):
    os.makedirs(NEWS_PATH)


TIMING_PATH = os.path.join(HOME_PATH, 'timing')
if not os.path.exists(TIMING_PATH):
    os.makedirs(TIMING_PATH)

for i in range(1,7):
    if not os.path.exists(TIMING_PATH+'/V{0}'.format(i)):
        os.makedirs(TIMING_PATH+'/V{0}'.format(i))

