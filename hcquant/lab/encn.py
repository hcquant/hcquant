import pandas as pd
import requests
from bs4 import BeautifulSoup

# word='汉字简体繁体转换'
def translate(word):
    url ='http://www.iciba.com/{0}'.format(word)
    web = requests.get(url)
    web.encoding='UTF8'
    soup=BeautifulSoup(web.text)
    res='';
    c=soup.select('.in-base-top')
    for k in c:
        res = k.select('div')[0].text
    return res
