# -*- coding: utf-8 -*-
# init system
from hcquant.utils import *
# 日期类函数
from hcquant.calendar import *
# 获取所有交易日期
get_tradeday()
# 交易日调整函数，向前向后移动
print(step_trade_date('20200913', 1))
print(step_trade_date('20200913', -2))
print(step_trade_date('20200913', 0))
# 调整为最近的交易日
adjust_to_trade_date('20200913')
# 给定开始日期和结束日期，返回其中的所有交易日
get_time_list('20190101', '20200108', astype='list')
get_time_list('20200101', '20200915', astype='pd')

# 获取所有的节假日,不包括周末
hos=get_holidays()

# 获取年月日的具体日期
# 提取历史上每个月月初的日期
m = Monthly(1)
m.get()

# 提取历史上每个月月末的日期
m = Monthly(-1)
m.get()

# 提取历史上每个周周初的日期
m = Weekly(1)
m.get()

# 提取历史上每个周周末的日期
m = Weekly(-1)
m.get()

# 提取历史上双个周周初的日期
m=BiWeekly(1)
m.get()

# 提取历史上双个周周末的日期
m=BiWeekly(-1)
m.get()

# 每个季度第一天
m=Quarterly(1)
m.get()

# 每个季度最后一天
m=Quarterly(-1)
m.get()

# 每年的第一天
m=Yearly(1)
m.get()

# 每年的最后一天
m=Yearly(-1)
m.get()

# 报告期后的第一天
m=Reportly(1)
m.get()


from hcquant.news import *
a=News()
b = a.get()
b = a.get(ticker='000413', trade_dt='20191213')
b = a.get(ticker='000413', begin_dt='20190101')
b = a.get(ticker='000413', begin_dt='20190101', end_dt='20190529')
b = a.get(ticker='000413', begin_dt='20190101', end_dt='20190529', kwds='下跌')



# 如本地有组合，可通过循环读取
codes = pd.read_csv('http://hcquant.com/hcquant-demo/portfolio.csv',dtype={'code':str})
allnews = pd.DataFrame()
for i in codes.code[:10]:
    # 加入上面的get函数
    news = a.get(i, trade_dt='20191213')
    allnews = allnews.append(news)  # 获取新闻的速度取决于查询股票的多少，请耐心等待

allnews.head()


# 择时信号
import pandas as pd
from hcquant.timing import *
timingdownSingal()

timingV5 = timing_deal(ver='v7',code='881001')
timingV5 = timing_deal(ver='v7',code='881001_tb')


import pyecharts.options as opts
from pyecharts.charts import Line

myplot = plottiming(timingV5)
myplot.render_notebook()
myplot.render()


# 基金
from hcquant.fund import *

# 基金基础信息
fundinfo = get_fund_info(['000762.OF'])
print_fund_info(fundinfo)
fund_manger = get_fund_manger(['000762.OF'])


#  基金净值信息与分析
fundnav = get_fund_nav(['000762.OF'])
analysisresults1=get_fund_nav_analysis(fundnav)
print(analysisresults1.T)
analysisresults2=get_fund_nav_analysis(fundnav, bdate='20200101') # 获取指定时间内的净值分析结果
print(analysisresults2.T)


# 基金资产配置分析



# 获取大师系列最新结果
from hcquant.master import *
master = get_master_performace()

# 获取最新vix的数值
from hcquant.vix import *
vix50 = get_vix('000016')
vix300 = get_vix('000300')
vix159919 = get_vix('159919')
vix510300 = get_vix('510300')

# 每日复盘
from hcquant.utils import *


# 小工具
# 天气
from hcquant.lab.weather import *
location='shanghai/pudong'
get_weather(location)

location='heilongjiang/hegang'
get_weather(location)

# 身份证归属地
from hcquant.lab.idcard import *
import warnings
warnings.filterwarnings("ignore")
id='230407198503150121'
getidinfo(id)

# 中英互译
from hcquant.lab.encn import *
import warnings
warnings.filterwarnings("ignore")
word1='吃葡萄不吐葡萄皮，不吃葡萄倒吐葡萄皮'
word2="""The star may dissolve, and the fountain of light
May sink intoe'er ending chao's and night,
Our mansions must fall and earth vanish away;
But thy courage, O Erin! may never decay.
See! the wide wasting ruin extends all around,
Our ancestors' dwellings lie sunk on the ground,
Our foes ride in triumph throughout our domains,
And our mightiest horoes streched on the plains.
Ah! dead is the harp which was wont to give pleasure,
Ah! sunk in our sweet country's rapturous measure,
But the war note is weaked, and the clangour of spears,
The dread yell of Slogan yet sounds in our ears.
Ah! where are the heroes! triumphant in death,
Convulsed they recline on the blood-sprinkled heath,
Or the yelling ghosts ride on the blast that sweeps by,
And my countrymen! vengeance! incessantly cry."""
print(translate(word1))
print(translate(word2))

# 全国按省份肺炎实时疫情
from hcquant.lab.pneumonia import *
data=pneumonia()


# 因子库调用
from hcquant.factorapi import FactorAPI
from hcquant.factorapi.orm import factorengine

# 获取当前所有因子的列表
factorlist = FactorAPI._get_factor_list()
use_factor_list = ['Beta', 'DASTD', 'Momentum', 'PB_MRQ', 'Mkt_Value', 'PE_TTM', 'STOM']
df_ = FactorAPI.get_factor(sid_list=['600000.SH','000001.SZ'], factor_list=use_factor_list, begin_dt='20190101', end_dt='20200107')
df_.tail(10)


