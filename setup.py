from setuptools import setup, find_packages

setup(
    name='hcquant',
    version='1.1',
    description=(
        'utils for hcquant'
    ),
    install_requires=[
        'bs4',
        'lxml', 
        'tushare',
       # 'cn-highcharts',
        'pymysql',
'pyecharts'
    ],
    # long_description=open('README.rst').read(),
    author='wangxiaochuan',
    author_email='wangxiaochuan@hcyjs.com',
    # maintainer='<维护人员的名字>',
    # maintainer_email='<维护人员的邮件地址',
    license='Apache Software License',
    packages=find_packages(),
    platforms=["all"],
    url='http://www.hcquant.com',
    classifiers=[
        'Operating System :: OS Independent',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Apache Software License',
        'Programming Language :: Python :: 3.6',
        'Topic :: Software Development :: Libraries'
    ],
)
