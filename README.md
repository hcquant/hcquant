hcquant V1.1 for Windows
========
HCQuant Public Python Tool 
说明文档最新更新时间：2020.9.14

“华创金工研究团队”推出并维护。


主要功能
========
* 时间处理
* 新闻舆情模块
* 择时模块
* 大师系列
* 基金分析工具
* 回测系统模块
* 多因子模块


安装说明
=======
代码对 Python 2/3 均兼容，请下载dist下的hcquant-1.1压缩包后进行安装

```
pip install hcquant-1.1

```


注意在使用hcquant模块前，请进行一次初始化工作
```
from hcquant.utils import *
```
初始化工作的主要作用的验证数据库账号与密码，同时在C盘user路径下建立hcquant为名称的文件夹，在文件夹中有支持后续工具运行的文件支持。


主要功能
=======
1. 时间处理
--------
工具列表：
* `hcquant.calendar` 为整个时间处理的总模块，在进行时间处理时可以全部导入；
* `get_tradeday` 获取所有交易日期；
* `step_trade_date` 交易日调整函数，向前向后移动；
* `adjust_to_trade_date` 调整为最近的交易日；
* `get_time_list` 给定开始日期和结束日期，返回其中的所有交易日；
* `get_holidays` 获取所有的节假日；
* `Monthly` 提取历史上每个月月初或者月末的日期；
* `Weekly` 提取历史上每个周周初或者周末的日期；
* `Weekly` 提取历史上每个周周初或者周末的日期；
* `BiWeekly` 提取历史上双周周初或者周末的日期；
* `Quarterly` 提取历史上每个季度初或者季度末的日期；
* `Yearly` 提取历史上每年年初或者年末的日期；
* `Reportly` 报告期后的第一天；

代码调用示例
```
# init system
from hcquant.utils import *
# 日期类函数
from hcquant.calendar import *
# 获取所有交易日期
get_tradeday()
# 交易日调整函数，向前向后移动
print(step_trade_date('20200913', 1))
print(step_trade_date('20200913', -2))
print(step_trade_date('20200913', 0))
# 调整为最近的交易日
adjust_to_trade_date('20200913')
# 给定开始日期和结束日期，返回其中的所有交易日
get_time_list('20190101', '20200108', astype='list') # 返回为list
get_time_list('20190101', '20200108', astype='pd')  # 返回为DataFrame
# 获取所有的节假日
get_holidays()
# 获取年月日的具体日期
# 提取历史上每个月月初的日期
m = Monthly(1)
m.get()
# 提取历史上每个月月末的日期
m = Monthly(-1)
m.get()
# 提取历史上每个周周初的日期
m = Weekly(1)
m.get()
# 提取历史上每个周周末的日期
m = Weekly(-1)
m.get()
# 提取历史上双个周周初的日期
m=BiWeekly(1)
m.get()
# 提取历史上双个周周末的日期
m=BiWeekly(-1)
m.get()
# 每个季度第一天
m=Quarterly(1)
m.get()
# 每个季度最后一天
m=Quarterly(-1)
m.get()
# 每年的第一天
m=Yearly(1)
m.get()
# 每年的最后一天
m=Yearly(-1)
m.get()
# 报告期后的第一天
m=Reportly(1)
m.get()
```

2. 新闻舆情模块
--------
### 此处注意，新闻舆情需要开通数据库支持。
如果需要，请联系wangxiaochuan@hcyjs.com。已经拿到数据库配置的朋友请将`setting.ini`文件放入`C:\user\你的计算机名\hcquant`文件夹下。
* `a=News()` 初始化一个实例，实例中的get函数为新闻获取函数；
* `a.get()` 获取当前交易日所有的负面舆情；
* `a.get(ticker='000413', trade_dt='20191213')` 获取指定股票，指定交易日的负面舆情；
* `a.get(ticker='000413', begin_dt='20190101', end_dt='20190529')` 获取指定股票指定日期间的负面舆情
* `a.get(ticker='000413', begin_dt='20190101', end_dt='20190529', kwds='下跌')`  获取指定股票指定日期间的负面舆情，并且标题包括某关键词；

代码调用示例
```
from hcquant.utils import engine
from hcquant.news import *
a=News()
b = a.get()
b = a.get(ticker='000413', trade_dt='20191213')
b = a.get(ticker='000413', begin_dt='20190101')
b = a.get(ticker='000413', begin_dt='20190101', end_dt='20190529')
b = a.get(ticker='000413', begin_dt='20190101', end_dt='20190529', kwds='下跌')
```
如本地有组合，可通过循环读取
```
codes = pd.read_csv('http://hcquant.com/hcquant-demo/portfolio.csv',dtype={'code':str})
allnews = pd.DataFrame()
for i in codes.code:
    # 加入上面的get函数
    news = a.get(i, trade_dt='20191213')
    allnews = allnews.append(news)  # 获取新闻的速度取决于查询股票的多少，请耐心等待

allnews.head()
```
结果为：
![Image text](https://gitee.com/hcquant/hcquant/raw/master/readme_pic/news.png)

3. 择时模块
-----------
一次性下载最新的择时数据与历史信号，可以在本地进行净值对比，分析历史看多信号等。华创量化的择时系统在线版地址为:`http://t.hcquant.com/`,本模块就是下载网站数据后进行本地分析。

代码调用示例
```
from hcquant.timing import *
timingdownSingal() #一次性下载所有择时模型信号
```
可以选择具体模型的信号
```
timingV5 = timing_deal(ver='v5',code='000001')
```
timingV5的数据为  
![Image text](https://gitee.com/hcquant/hcquant/raw/master/readme_pic/timingdataframe.png)

在Jupyter Notebook下可以画图
```
import pyecharts.options as opts
from example.commons import Faker
from pyecharts.charts import Line
myplot = plottiming (timingV5)
myplot.render_notebook()
```

![Image text](https://gitee.com/hcquant/hcquant/raw/master/readme_pic/timingcompare.png)

4. 大师系列
-----------
### 获取大师系列最新结果
```
from hcquant.master import *
master = get_master_performace()
```
结果：  
![Image text](https://gitee.com/hcquant/hcquant/raw/master/readme_pic/master.png)

5. VIX数据获取
-----------
### 获取最新vix的数值，数据会直接下载到本地
```
from hcquant.vix import *
vix50 = get_vix('000016')
vix300 = get_vix('000300')
vix159919 = get_vix('159919')
vix510300 = get_vix('510300')
```

6. 单个基金分析模块
------------------
#### 指定基金，获取基金经理等发行信息，并进行净值分析
```
from hcquant.fund import *
# 基金基础信息
fundinfo = get_fund_info(['000762.OF'])
print_fund_info(fundinfo)
fund_manger = get_fund_manger(['000762.OF'])
```
输出：
```
*******FundBasic**********
基金代码:000762.OF
基金名称:汇添富绝对收益策略定期开放混合型发起式证券投资基金
基金简称:汇添富绝对收益策略A
基金管理人:汇添富基金
基金托管人:招商银行
基金投资类型:混合型
基金成立日期:20170315
基金到期日期:None
基金管理费:1.5
基金托管费:0.25
基金投资风格:混合型
基金投资理念:None
基金投资目标:基于基金管理人对市场机会的判断，灵活应用包括市场中性策略在内的多种绝对收益策略，在严格的风险管理下，追求基金资产长期持续稳定的绝对回报。
基金投资策略:本基金采用包括市场中性投资策略在内的多种绝对收益策略，力争实现基金资产长期持续稳定的绝对回报。1、绝对收益策略(1)市场中性投资策略本基金通过持续、系统、深入的基本面研究构建多头股票组合，并匹配合适的空头工具，对冲多头股票组合的系统性风险，从而实现较低风险的稳健回报。本策略主要包括两个方面：多头股票投资策略和空头工具投资策略，多头股票投资策略是市场中性策略的核心，在本基金管理人统一的价值投资理念基础上，立足于深入的基本面分析，精选高质量证券组合。空头工具投资策略，主要运用股指期货对冲策略，并根据对冲的需要，对多头股票组合进行贝塔、风格配置管理。1)多头股票投资策略本基金通过系统地分析中国证券市场的特点和运作规律，以深入的基本面研究为基础，自下而上地通过定量和定性相结合的方法，考察上市公司的治理结构、核心竞争优势、议价能力、市场占有率、成长性、盈利能力、运营效率、财务结构、现金流情况以及公司基本面变化等，并对上市公司的投资价值进行综合评价，精选具有较高投资价值的上市公司构建多头组合。2)空头工具投资策略本基金将主要利用股指期货对冲多头股票部分的系统性风险。本基金根据市场的变化、现货市场与期货市场的相关性等因素，计算需要用到的期货合约数量，对这个数量进行动态跟踪与测算，并进行适时灵活调整。同时，综合考虑各个月份期货合约之间的定价关系、套利机会、流动性以及保证金要求等因素，在各个月份期货合约之间进行动态配置，通过空头部分的优化创造额外收益。随着监管的放开及其他工具流动性增强，本基金会优选空头工具，采用最优的多头系统性风险对冲方式。(2)、其他绝对收益策略本基金采用股指期货套利的对冲策略寻找和发现市场中资产定价的偏差，捕捉绝对收益机会。同时，本基金根据市场情况的变化，灵活运用其他绝对收益策略(如统计套利策略、定向增发套利策略、大宗交易套利、并购套利策略等)提高资本的利用率，从而提高绝对收益，达到投资策略的多元化以进一步降低本基金收益的波动。2、其他投资策略(1)、债券投资策略债券投资是本产品的有益补充。本基金投资于债券品种，包括国内依法公开发行和上市的国债、金融债、公司债、可转债、央行票据和资产支持证券等；此外，市场未来推出的各类债券及其衍生品也可能成为本基金的投资对象。本基金的债券投资综合考虑收益性、风险性和流动性，在深入分析宏观经济、货币政策以及市场结构的基础上，灵活运用各种消极和积极策略。(2)、中小企业私募债投资策略对于中小企业私募债券，本基金的投资策略以持有到期为主，在综合考虑债券信用资质、债券收益率和期限的前提下，重点选择资质较好、收益率较好、期限匹配的中小企业私募债券进行投资。基金投资中小企业私募债券，基金管理人将根据审慎原则，制定严格的投资决策流程、风险控制制度和信用风险、流动性风险处置预案，并经董事会批准，以防范信用风险、流动性风险等各种风险。(3)、衍生品投资策略本基金的衍生品投资将严格遵守证监会及相关法律法规的约束，合理利用期货、期权、权证等衍生工具，利用数量方法发掘可能的套利机会。投资原则为有利于基金资产增值，控制下跌风险，实现保值和锁定收益。
*******FundBasic End**********
```
基于基金净值计算历史表现：
```
fundnav = get_fund_nav(['000762.OF'])
analysisresults=get_fund_nav_analysis(fundnav)
print(analysisresults.T)
```
输出：
```
maxdrawdown  -0.058475
annualreturn  0.111448
calmarratio   1.905914
omegaratio    1.438508
sharperatio   2.009317
sortinoratio  3.171630
downsiderisk  0.033825
tailratio     1.248820
```


7. 因子数据库模块
------------------
#### 指定股票日度因子值
```
# 因子库调用
from hcquant.factorapi import FactorAPI
# 获取当前所有因子的列表
factorlist = FactorAPI._get_factor_list()
# 获取指定因子因子值
use_factor_list = ['Beta', 'DASTD', 'Momentum', 'PB_MRQ', 'Mkt_Value', 'PE_TTM', 'STOM']
df_ = FactorAPI.get_factor(sid_list=['600000.SH','000001.SZ'], factor_list=use_factor_list, begin_dt='20190101', end_dt='20200107')
df_.tail(10)
```
输出：
![Image text](https://gitee.com/hcquant/hcquant/raw/master/readme_pic/factorsdb.png)


